//
//  File.swift
//  
//
//  Created by Nabnit Patnaik on 8/17/22.
//

import Foundation

public enum NetworkError: Error {
    case parsingFailed
    case badRequest
}

public class Webservice {
    public init() {}
    public func fetch<T: Codable>(url: URL, parse: @escaping ((Data)-> T?), completion: @escaping (Result<T?, NetworkError>) -> Void) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil, (response as? HTTPURLResponse)?.statusCode == 200 else {
                completion(.failure(.badRequest))
                return
            }
            let result = parse(data)
            completion(.success(result))
        }.resume()
    }
}


